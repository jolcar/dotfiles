<h3>Dotfiles</h3>

<p><i>This is a snapshot of my dotfiles.  It is intended for my personal use, feel free to implement parts of my config files as your own if you find something helpful.</i></p>

<b>Distribution: Arch Linux / Window Manager: i3-gaps</b>

List of useful tools/packages:</p>

<ul>
  
  <li>Bumblebee-status</li>
  <li>Rofi application launcher</li>
  <li>Compton compositor</li>
  <li>Xfce4-terminal</li>
  <li>Spacemacs/Vim/IntelliJ IDEA/VSCode</li>
  <li>Thunar/Ranger file managers</li>
  <li>Powerline</li>
  <li>Nitrogen</li>
  <li>Qutebrowser/Firefox</li>
</ul>
  
  
