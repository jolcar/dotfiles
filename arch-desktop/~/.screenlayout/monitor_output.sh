#!/bin/sh
xrandr --output DisplayPort-0 --off --output HDMI-1-2 --off --output HDMI-1-1 --off --output VGA-1-1 --off --output DVI-1 --mode 1920x1080 --pos 3640x0 --rotate left --output DVI-0 --primary --mode 2560x1440 --pos 1080x0 --rotate normal --output HDMI-2 --mode 1920x1080 --pos 0x0 --rotate right
